#include "datag.h"
#include "utilsf.h"
#include <sstream>

const int MAX_COUNT = 100;
cv::TermCriteria termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03);
cv::Size winSize(31,31);

datag::datag(const cv::Mat_<float> cameraMatrix)
{
    K = cameraMatrix;
}

void datag::inputFrames(const cv::Mat &frame1, const cv::Mat &frame2)
{
    f1 = frame1;
    f2 = frame2;

    flow.clear();
    pts.clear();
}

void datag::outputData(std::vector< cv::Point2f > &out_pts, std::vector< cv::Point2f > &out_flow)
{

    //scaling point set and flow vector
    for (size_t i = 0; i < pts.size(); i++)
    {
        flow[i].x = (flow[i].x - pts[i].x) / K(0,0);
        flow[i].y = (flow[i].y - pts[i].y) / K(1,1);
        pts[i].x -= (K(0,2));
        pts[i].x = pts[i].x / K(0,0);
        pts[i].y -= (K(1,2));
        pts[i].y = pts[i].y / K(1,1);
    }

    //output
    out_pts = pts;
    out_flow = flow;

    std::cout << "number of points remaining are: " << pts.size() << std::endl;
}


void datag::calcFlow(void)
{
    //detect features to obtian point set
    std::vector< cv::Point2f > ori_pts, next_pts;
    
    cv::goodFeaturesToTrack(f1, ori_pts, MAX_COUNT, 0.01, 10, cv::Mat(), 3, 0, 0.04);

    //calculate flow for point set
    std::vector< uchar > vstatus;
    std::vector< float > verror;

    cv::calcOpticalFlowPyrLK(f1, f2, ori_pts, next_pts, vstatus, verror, cv::Size(50, 50), 5);

    std::cout << "opticla flow computed" << std::endl;

    for (size_t i = 0; i < ori_pts.size(); i++)
    {
        if (vstatus[i] && (verror[i] < 4.0))
        {
            pts.push_back(ori_pts[i]);
            flow.push_back(next_pts[i]);
        }
    }

    std::cout << "number of points remaining are: " << pts.size() << std::endl;

    //draw flow vectors
    if (pts.size() != flow.size())
        assert(0);
    cv::Mat drawf = f1.clone();
    for (size_t i = 0; i < flow.size(); i++)
    {
        drawArrows(drawf, pts[i], flow[i]);
    }

    cv::imshow("flow vector", drawf);
    cv::waitKey(20);
}



#ifndef DATAG_H
#define DATAG_H

#include <opencv2/opencv.hpp>

class datag
{
private:
    cv::Mat f1, f2;
    cv::Mat_<float> K;
    std::vector< cv::Point2f > flow;
    std::vector< cv::Point2f > pts;

    std::vector< cv::Point2f > points[2];

public:
    datag(const cv::Mat_<float> cameraMatrix);
    void inputFrames(const cv::Mat &frame1, const cv::Mat &frame2);
    void calcFlow(void);
    void outputData(std::vector< cv::Point2f > &out_pts, std::vector< cv::Point2f > &out_flow);
};

#endif // DATAG_H

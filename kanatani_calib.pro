TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    cvcapture_gstreamer.cpp \
    datag.cpp \
    ego.cpp \
    utilsf.cpp

HEADERS += \
    cvcapture_gstreamer.h \
    datag.h \
    ego.h \
    utilsf.h

LIBS += \
    `pkg-config --libs opencv gstreamer-0.10               gstreamer-net-0.10\
                        gstreamer-app-0.10           gstreamer-netbuffer-0.10\
                        gstreamer-audio-0.10         gstreamer-pbutils-0.10\
                        gstreamer-base-0.10          gstreamer-plugins-base-0.10\
                        gstreamer-cdda-0.10          gstreamer-riff-0.10\
                        gstreamer-check-0.10         gstreamer-rtp-0.10\
                        gstreamer-controller-0.10    gstreamer-rtsp-0.10\
                        gstreamer-dataprotocol-0.10  gstreamer-sdp-0.10\
                        gstreamer-fft-0.10           gstreamer-tag-0.10\
                        gstreamer-floatcast-0.10     gstreamer-video-0.10\
                        gstreamer-interfaces-0.10    `

HEADERS += \
    ego.h \
    datag.h \
    utilsf.h \
    Client.h \
    cvcapture_gstreamer.h

INCLUDEPATH += /usr/local/include/eigen3/ \
    /usr/include/gstreamer-0.10\
    /usr/include/glib-2.0\
    /usr/lib/i386-linux-gnu/glib-2.0/include\
    /usr/include/libxml2

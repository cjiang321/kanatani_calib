#ifndef UTILSF_H
#define UTILSF_H

#include <opencv2/opencv.hpp>


int drawArrows(cv::Mat& frame,
               const cv::Point2f& prevPts,
               const cv::Point2f& nextPts);

#endif // UTILSF_H

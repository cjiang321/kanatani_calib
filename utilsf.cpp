#include "utilsf.h"

int drawArrows(cv::Mat& frame,
               const cv::Point2f& prevPts,
               const cv::Point2f& nextPts)
{
    int line_thickness = 1;
    cv::Scalar line_color(0, 255, 0);

    cv::Point p = prevPts;
    cv::Point q = nextPts;

    float angle = atan2((float) p.y - q.y, (float) p.x - q.x);
    float hypotenuse = sqrt( (float)(p.y - q.y)*(p.y - q.y) + (float)(p.x - q.x)*(p.x - q.x) );

    if (hypotenuse < 1.0)
        return 1;

    // Here we lengthen the arrow by a factor of three.
    /*
     * q.x = (int) (p.x - 3 * hypotenuse * cos(angle));
     * q.y = (int) (p.y - 3 * hypotenuse * sin(angle));
    */
    // Now we draw the main line of the arrow.
    cv::line(frame, p, q, line_color, line_thickness);

    // Now draw the tips of the arrow. I do some scaling so that the
    // tips look proportional to the main line of the arrow.
    p.x = (int) (q.x + 9 * cos(angle + CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle + CV_PI / 4));
    cv::line(frame, p, q, line_color, line_thickness);

    p.x = (int) (q.x + 9 * cos(angle - CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle - CV_PI / 4));
    cv::line(frame, p, q, line_color, line_thickness);

    return 0;
}

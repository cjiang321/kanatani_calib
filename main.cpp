#include <iostream>
#include <sstream>
#include <fstream>

#include "ego.h"
#include "datag.h"
#include "cvcapture_gstreamer.h"


using namespace std;

int main(int argc, char* argv[])
{
    //parsing argument
    if (argc != 2)
    {
        cout << "Error! argument needed, exit" << endl;
        assert(0);
    }

    istringstream istr(argv[1]);
    float num = 0.0;
    if (!(istr >> num))
            num = 0.0;

    if (num == 0.0)
    {
        cout << "Error! shoudn't be 0, exit" << endl;
        assert(0);
    }

    /*///////////////////////////*/

    cv::Matx33f Kv(5.8781933868326530e+02, 0, 3.3127983417951896e+02,
              0, 5.8910513442917556e+02, 2.3486662037824394e+02,
              0, 0, 1);
    cv::Mat_<float> K = cv::Mat_<float>(Kv);

    //variables
    cv::Point3f calib_v(num, 0, 0);

    vector< cv::Point2f > flow;
    vector< cv::Point2f > pts;

    float f = (K(0,0) + K(1,1)) / 2;

    cv::Point3f v,r;
    float depth;

    datag genData(K);

    //initialise file for data logging
    ofstream fout;
    fout.open("calib.txt");

    CvCapture_GStreamer gst_cap;

    int type = CV_CAP_GSTREAMER_IMXV4L;
    //int type = CV_CAP_GSTREAMER_V4L2;

    if (!gst_cap.open(type, NULL))
    {
        cout << "fail open device with gstreamer" << endl;
        return -1;
    }

    for (int i = 0; i < 20; i++){
        gst_cap.grabFrame();
        cv::waitKey(100);
        gst_cap.retrieveFrame(0);
    }

    IplImage *iplim;
    cv::Mat img1, img2;

    double state;
    state = gst_cap.getProperty(CV_CAP_PROP_FRAME_HEIGHT);
    cout << "frame height is: " << state << endl;
    state = gst_cap.getProperty(CV_CAP_PROP_FRAME_WIDTH);
    cout << "frame width is:  " << state << endl;
    state = gst_cap.getProperty(CV_CAP_PROP_FPS);
    cout << "current fps is:  " << state << endl;

    //cout << "reading images..." << endl;
    //waitkey
    char c = ' ';
    while (c != 'c')
        cin >> c;

    gst_cap.grabFrame();
    iplim = gst_cap.retrieveFrame(1);
    cv::cvtColor(cv::Mat(iplim), img1, CV_RGB2GRAY);
    cv::waitKey(20);

    //waitkey
    c = ' ';
    while (c != 'c')
        cin >> c;

    gst_cap.grabFrame();
    iplim = gst_cap.retrieveFrame(1);
    cv::cvtColor(cv::Mat(iplim), img2, CV_RGB2GRAY);
    cv::waitKey(20);

    //first computation of known movement to initialise height parameter
    //cout << "height calibration..." << endl;

    genData.inputFrames(img1, img2);
    genData.calcFlow();
    genData.outputData(pts, flow);

    ego egomotion(flow, pts, f);
    egomotion.kanatani(v, r, depth);

    float ratio = cv::norm(calib_v) / cv::norm(v);
    float realheight = fabs(depth) * ratio;
    
    cout << "###########################" << endl
         << "height written" << endl
         << "----------------" << endl
         << realheight << endl
         << "###########################" << endl;

    fout << realheight << endl;
    fout.close();

    while (cv::waitKey(20) != ' ');
    cv::destroyAllWindows();

    return 0;
}

